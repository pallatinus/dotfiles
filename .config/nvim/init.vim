" Set Leader key
let mapleader="`"

" urlview - maps to /u
:noremap <leader>u :w<Home>silent <End> !urlview<CR>

" Set spell check
map <leader>s :setlocal spell! spelllang=en_us<CR>

" Splits open at the bottom and right
set splitbelow splitright

" Check file in shellcheck:
map <leader>p :!clear && shellcheck %<CR>

" Save file as sudo on files that require root permission
cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!

" Toggle paste
set pastetoggle=<F2>

" Automatic text replacement
map S :%s//gI<Left><Left><Left>

" Disable autocomments on new lines
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Ignore case sensitivity and do a smart search
set ignorecase
set smartcase

" Change Tabs for spaces
set expandtab

" Do not create a backup file
set nobackup

" Set line numbers
:set number
syntax on

" Close Brackets
inoremap " ""<left>
inoremap ' ''<left>
inoremap ( ()<left>
inoremap [ []<left>
inoremap { {}<left>
inoremap {<CR> {<CR>}<ESC>O
inoremap {;<CR> {<CR>};<ESC>O

" Set cursor line
set cursorline

" colorscheme onedark

" Set Tab completion for CoC
" Use tab for trigger completion with characters ahead and navigate
" NOTE: There's always complete item selected by default, you may want to enable
" no select by `"suggest.noselect": true` in your configuration file
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config
inoremap <silent><expr> <TAB>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

execute pathogen#infect()
filetype plugin indent on

"pathogen & Live-Latex auto compile"

